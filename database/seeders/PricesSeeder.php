<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class SettingSeeder
 */
class PricesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $prices = [
            1 => [
                'amount'=>100000,
                'price'=>0.1
            ],
            2 => [
                'amount'=>80000,
                'price'=>0.2
            ],
            3 => [
                'amount'=>64000,
                'price'=>0.4
            ],
            4 => [
                'amount'=>51200,
                'price'=>0.8
            ],
            5     => [
                'amount'=>40960,
                'price'=>1.6
            ],
            6      => [
                'amount'=>32768,
                'price'=>3.2
            ],
            7  => [
                'amount'=>26214,
                'price'=>6.4
            ],
            8      => [
                'amount'=>20971,
                'price'=>12.8
            ],
            9  => [
                'amount'=>16777,
                'price'=>25.6
            ],
            10   => [
                'amount'=>13422,
                'price'=>51.2
            ],
        ];

        foreach ($prices as $key => $arr) {
            $checkExists = DB::table('price_ups')->where('id', $key)->count();

            if ($checkExists > 0) {
                echo "Price '".$key."' already registered.\n";
                continue;
            }

            DB::table('price_ups')->insert([
                'id'=>$key,
                'amount' => $arr['amount'],
                'price' => $arr['price'],
            ]);
            echo "Price '".$key."' registered.\n";
        }
    }
}
