<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinDaysCoef extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumns('deposits', ['min_days', 'start_percent', 'max_fst'])) {
            Schema::table('deposits', function (Blueprint $table) {
                $table->integer('min_days')->default(0);
                $table->integer('start_percent')->default(0);
                $table->integer('max_fst')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumns('deposits', ['min_days', 'start_percent', 'max_fst'])) {
            Schema::table('deposits', function (Blueprint $table) {
                $table->dropColumn('min_days');
                $table->dropColumn('start_percent');
                $table->dropColumn('max_fst');
            });
        }
    }
}
