<?php
return [
    'order_created'       => 'A ordem #:id :amount :currency foi criada com êxito',
    'order_closed'        => 'A ordem #:id :amount :currency foi fechada por causa de ausência dos fundos no saldo',
    'sale'                => 'A venda :amount :currency foi realizada com êxito',
    'purchase'            => 'A compra :amount :currency foi realizada com êxito',
    'partner_accrue'      => 'O Senhor (A Senhora) acaba de receber a comissão de sócio :amount :currency do usuário :login, no nível :level',
    'wallet_refiled'      => 'Seu porta-moedas foi recarregado por :amount :currency',
    'rejected_withdrawal' => 'Seu retiro a um montante de :amount :currency foi cancelado.',
    'approved_withdrawal' => 'Seu retiro a um montante de :amount :currency foi confirmado.',
    'new_partner'         => 'O Senhor (A Senhora) tem um novo sócio :login no nível :level',
    'parking_bonus'       => 'Bono por parking :amount :currency',
    'licence_cash_back'   => 'Cashback por la compra de una licencia :amount :currency'
];