<?php
namespace App\Http\Middleware;

use App\Models\AuthAttempts;
use Closure;

class PageViews

{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        \App\Models\PageViews::addRecord();

        /** @var AuthAttempts $checkBase */
//        $checkBase = AuthAttempts::where('ip', $_SERVER['REMOTE_ADDR'])->first();
//
//        if (null !== $checkBase) {
//            if ($checkBase->isBlocked()) {
//                die('...');
//            }
//        }
//
//        $checkSpam = \App\Models\PageViews::where('user_ip', $_SERVER['REMOTE_ADDR'])
//            ->where('created_at', '>=', now()->subHours(1))
//            ->count();
//
//        if ($checkSpam >= env('CHECK_SPAM_LIMIT', 500)) {
//            AuthAttempts::create([
//                'ip'        => $_SERVER['REMOTE_ADDR'],
//                'attempts'  => 1,
//                'blocked'   => true,
//            ]);
//            die('...');
//        }

        return $response;
    }
}
