<?php
namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Spatie\RobotsMiddleware\RobotsMiddleware;

class MyRobotsMiddleware extends RobotsMiddleware
{
    /**
     * @param Request $request
     * @return bool|string
     */
    protected function shouldIndex(Request $request)
    {
        return $request->segment(1) !== 'admin'
            && (config('app.env') == 'production' || config('app.env') == 'demo');
    }
}