<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestSaveUserSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class SettingsController
 * @package App\Http\Controllers\Profile
 */
class SettingsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // Initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // Add the secret key to the registration data
        $registration_data["google2fa_secret"] = $google2fa->generateSecretKey();

        // Save the registration data to the user session for just the next request
        session()->flash('registration_data', $registration_data);

        // Generate the QR image. This is the image the user will scan with their app
        // to set up two factor authentication
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            \Auth::user()->email,
            $registration_data['google2fa_secret']
        );

        return view('profile.settings', [
            'captchaImg' => captcha_img(),
            'QR_Image' => $QR_Image,
            'secret' => $registration_data['google2fa_secret']
        ]);
    }

    /**
     * @param RequestSaveUserSettings $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(RequestSaveUserSettings $request)
    {
        /** @var \App\Models\User $user */
        $user = \Auth::user();

        /*
         * Base settings
         */
        if ($request->has('name')) {
            $user->name = htmlspecialchars(trim($request->name));
        }

        if ($request->has('login')) {
            $user->login = $request->login;
        }

        if ($request->has('partner_id')) {
            $user->partner_id = $request->partner_id;
        }

        if ($request->has('phone')) {
            $user->phone = $request->phone;
        }

        if ($request->has('skype')) {
            $user->skype = htmlspecialchars(trim($request->skype));
        }


        if ($request->has('partner_text')) {
            $user->partner_text = htmlspecialchars(trim($request->partner_text));
        }

        if ($request->has('password') && $request->has('new_password') && $request->has('repeat_password')) {
            $password           = $request->password;
            $new_password       = $request->new_password;
            $repeat_password    = $request->repeat_password;

            if (false === Hash::check($password, $user->password)) {
                return redirect()->route('profile.settings')->with('error', __('Old password entered incorrectly'));
            }

            if ($new_password !== $repeat_password) {
                return redirect()->route('profile.settings')->with('error', __('Repeated password is incorrect'));
            }

            if ($new_password == $password) {
                return redirect()->route('profile.settings')->with('error', __('New password should be not same as old one'));
            }

            $user->setPassword($new_password);

            return redirect()->route('profile.settings')->with('success', __('Password saved successfully'));
        }

        $user->save();

        if (isset($_FILES['avatar'])) {
            $user->updateMedia([], 'avatars');
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
            return redirect()->route('profile.settings')->with('success', __('Avatar uploaded'));
        }

        /*
         * Wallets
         */
        if ($request->has('wallets')) {
            foreach ($request->wallets as $walletId => $walletAddress) {


                $wallet = $user->wallets()
                    ->where('id', $walletId)
                    ->first();

                if (null == $wallet) {
                    continue;
                }

                $walletAddress = htmlspecialchars(trim($walletAddress));

                if (!empty($walletAddress) && $walletAddress != $wallet->external) {
                    $wallet->external = $walletAddress;
                    $wallet->save();
                }
            }
            return redirect()->route('profile.settings')->with('success', __('Wallets was updated'));
        }


        return redirect()->route('profile.settings')->with('success', __('Settings successfully saved!'));
    }

    public function google2fa(Request $request)
    {
//        // add the session data back to the request input
//        $request->merge(session('registration_data'));

        /** @var \App\Models\User $user */
//        \Log::critical(print_r($request->all(), 1));
        $user = user();
        if ($request->google_2fa==1 && $request->has('google2fa_secret')) {
            $user->setGoogle2FaEnabled($request->google2fa_secret);
            session()->put('google_2fa_just_enabled', true);
            return redirect()->route('profile.settings')->with('success', __('Google authenticator enabled'));
        } elseif(!$request->has('google_2fa')||$request->google_2fa==0 ) {
            $user->setGoogle2FaDisabled();
            session()->remove('2fa_passed');
            return redirect()->route('profile.settings')->with('success', __('Google authenticator disabled'));
        }

        return redirect()->route('profile.settings')->with('success', __('2FA settings successfully saved!'));
    }
}
