<?php
namespace App\Console\Commands\Manual;

use App\Models\Currency;
use App\Models\Wallet;
use Illuminate\Console\Command;

/**
 * Class MergeWalletsCommand
 * @package App\Console\Commands\Manual
 */
class MergeWalletsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merge:wallets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Merge wallets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $wallets = Wallet::get();

        foreach ($wallets as $wallet) {
            $checkExists = Wallet::find($wallet->id);

            if ($checkExists == null) {
                continue;
            }

            foreach(Wallet::where('user_id', $wallet->user_id)->where('currency_id', $wallet->currency_id)->where('id', '!=', $wallet->id)->get() as $otherWallet) {
                $wallet->balance += $otherWallet->balance;
                $wallet->save();

                $otherWallet->delete();

                $this->info('processed wallets for user '.$wallet->user->email);
            }
        }
    }
}
