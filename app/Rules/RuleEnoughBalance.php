<?php
namespace App\Rules;

use App\Models\Licences;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RuleEnoughBalance
 * @package App\Rules
 */
class RuleEnoughBalance implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $wallet = getUserWallet('WEC');

        if ($wallet===null) {
            return false;
        }



        $amount = $value;


        if (\request()->has('licence_id'))
        {


            $wallet = user()->wallets()->where('id', request()->wallet_id)->first();

            if ($wallet===null) {
                return false;
            }

            $licence = Licences::find(request()->licence_id);
            if ($licence===null)
            {
                return false;

            }

            $amount = $licence->price*rate('USD',$wallet->currency->code);
        }

        return $wallet ? $wallet->balance >= $amount : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.enough_balance');
    }
}
