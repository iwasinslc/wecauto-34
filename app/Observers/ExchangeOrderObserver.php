<?php
namespace App\Observers;

use App\Models\Currency;
use App\Models\ExchangeOrder;

/**
 * Class CurrencyObserver
 * @package App\Observers
 */
class ExchangeOrderObserver
{
    /**
     * @param Currency $currency
     */
    public function deleting(ExchangeOrder $order)
    {

    }

    /**
     * Listen to the Currency created event.
     *
     * @param Currency $currency
     * @return void
     * @throws
     */
    public function created(ExchangeOrder $order)
    {
        clearCacheByArray($this->getCacheKeys($order));
        clearCacheByTags($this->getCacheTags($order));
    }

    /**
     * @param ExchangeOrder $order
     * @return array
     */
    private function getCacheKeys(ExchangeOrder $order): array
    {
        return [
            'sellLimit.'.$order->user_id,
            'buyLimit.'.$order->user_id
        ];
    }

    /**
     * @param Currency $currency
     * @return array
     */
    private function getCacheTags(ExchangeOrder $order): array
    {
        return [
            'OrdersTable',
            'OrdersTableCommon',
            'MyOrders',
            'MyOrdersBalance'
        ];
    }

    /**
     * Listen to the Currency deleting event.
     *
     * @param Currency $currency
     * @return void
     * @throws
     */
    public function deleted(ExchangeOrder $order)
    {
        clearCacheByArray($this->getCacheKeys($order));
        clearCacheByTags($this->getCacheTags($order));
    }

    /**
     * Listen to the Currency updating event.
     *
     * @param Currency $currency
     * @return void
     * @throws
     */
    public function updated(ExchangeOrder $order)
    {
        clearCacheByArray($this->getCacheKeys($order));
        clearCacheByTags($this->getCacheTags($order));
    }
}